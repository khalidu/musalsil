/*
      __                                                 __
     |_ |    ___                 ___                    |_ |    ___   ___
      /_/   |   |               |   |                    /_/   |   | |   |
            |   |  ___ ___ ___  |   |  ___ ___ ___    _______  |   | |   |
     ___    |   | |   |   |   | |   | |   |   |   |  /   _   | |   | |   |
    |   |   |   |_|   |   |   |_|   |_|   |   |   |_|   | |  |_|   | |   |
    |   |___|                                           |_|        | |   |
    |            _/___/___/___/_____/_/___/___/___/__________/_____/ |___|
    |___________/  Khalid Umar <khalid.umar@zoho..com> مع تحياتي، خالد عمر
    

    Copyright (c) 2013  Adnal Technologies, Kuwait

    This work is licensed under the terms and conditions of the Adnal-WPL 
    (Adnal Waqf Public License) which should be included with this program. 
    You can obtain a copy by visiting: http://adnal.net.kw/legal/wpl

    Compliance with all terms and conditions set forth in the Adnal-WPL 
    license is a requirement to exercise the rights it defines. If you are 
    unable to fully comply or are unwilling to do so, you may not exercise 
    the rights defined therein.

    This work is provided WITHOUT WARRANTY. The right holders DISCLAIM ANY 
    DAMAGE that may arise from the use of this work. USE AT YOUR OWN RISK.

    If translations of the Adnal-WPL license exist, the Arabic version is 
    taken to be correct and legally binding.

*//*
    TODO
        add mid loop modification check.
    
    Libary policies:
        - when iterating over a list, do not use length as a means of checking
          bounds
    
    Testing policy: NOT IINFORCED YET
        - test if functions can handle null lists
        - test if functions can handle single-node lists
        - test if functions can handle operations at both list ends
        - test lengths of results
        - transvers check
        
*/
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include "musalsil.h"

#ifdef MUSALSIL_EMBEDED_TEST
#include <assert.h>

static bool checklength(mslsl_t **list, int length){
    int counter;
    mslsl_node_t *n;
    
    if (list[0]->length != (unsigned int)length) {
        fprintf(stderr, "supply length does not match (lengths: %d, %d)\n", list[0]->length, length);
        return false;
    }
    n = list[0]->first;
    for (counter=0;n;n=n->next,++counter);
    if (counter != length) {
        fprintf(stderr, "forward list tansversal failed (successful transversals: %d of %d)\n", counter, length);
        return false;
    }
    n = list[0]->last;
    for (counter=0;n;n=n->prev,++counter);
    if (counter != length) {
        fprintf(stderr, "backward list tansversal failed (successful transversals: %d of %d)\n", counter, length);
        return false;
    }    
    return true;
}

static void printls(mslsl_t *list, char *prepend){ // DO NOT USE, FRBUGINH ONLY
    if(prepend) {
        fputs(prepend, stdout); 
        fputs(": ", stdout); 
    }
    for (mslsl_node_t *p = list->first; p; p = p->next) printf("%s ", (char *) p->ptr); 
    puts("");
}
#endif


/*
      __                                                 __
     |_ |    ___                 ___                    |_ |    ___   ___
      /_/   |   |               |   |                    /_/   |   | |   |
            |   |  ___ ___ ___  |   |  ___ ___ ___    _______  |   | |   |
     ___    |   | |   |   |   | |   | |   |   |   |  /   _   | |   | |   |
    |   |   |   |_|   |   |   |_|   |_|   |   |   |_|   | |  |_|   | |   |
    |   |___|                                           |_|        | |   |
    |            _/___/___/___/_____/_/___/___/___/__________/_____/ |___|
    |___________/  Khalid Umar <khalid.umar@zoho..com> مع تحياتي، خالد عمر
    
*/

bool mslslInsertNA(mslsl_t **list, void *ptrs[], int index){/*
USAGE
    This function inserts or appends a null-terminated array of pointers 
    in/to a linked list. It also initializes a new list if no list is 
    provided (*list == NULL).
    
PARAMETER DESCRIPTION
    list        This must be an address of a linked list pointer which can 
                point to an initialized list, or NULL. if `*list' is null, a 
                new list is initialized and the pointer is inserted into it.
    ptrs        A null-terminated array of pointers that should be inserted.
    index       The location at which `ptr' should be inserted. if `index' is
                -1, the ptr is appended to to the list.

FAILURE
    This function may fail if index is out of bounds, or due memory issues.
    
RETURN
    This function returns true on success and false on failure.
*/
    for (;*ptrs;++ptrs) {
        if(!mslslInsert(list, *ptrs, index)) return false; 
        if (index != -1) ++index;
    }
    return true;
}
#ifdef MUSALSIL_EMBEDED_TEST
static void test_mslslInsertNA(){
    mslsl_t *c = NULL;

    assert(mslslInsertNA(&c, (void *[]) {"1", "2", "3", NULL}, -1));
    assert(checklength(&c, 3));
    assert(mslslInsertNA(&c, (void *[]) {"7", "8", "9", NULL}, -1));
    assert(checklength(&c, 6));
    assert(mslslInsertNA(&c, (void *[]) {"4", "5", "6", NULL}, 3));
    assert(checklength(&c, 9));
    
    
    char z = '1';
    MSLSL_FOR(&c) {
        assert(*((char *)MSLSL_PTR) == z);
        ++z;
    }
    
    mslslFree(&c);
    
    assert(mslslInsertNA(&c, (void *[]) {NULL}, -1));
    assert(!c);    
    
}
#endif






bool mslslInsert(mslsl_t **list, void *ptr, int index){/*
USAGE
    This function inserts or appends a pointer in/to a linked list. It also 
    initializes a new list if no list is provided (*list == NULL).
    
PARAMETER DESCRIPTION
    list        This must be an address of a linked list pointer which can 
                point to an initialized list, or NULL. if `*list' is null, a 
                new list is initialized and the pointer is inserted into it.
    ptr         The pointer to be inserted.
    index       The location at which `ptr' should be inserted. if `index' is
                -1, the ptr is appended to to the list.

FAILURE
    This function may fail if index is out of bounds, or due memory issues.
    
RETURN
    This function returns true on success and false on failure.
*/
    if (!list[0]) {
        if (!(list[0] = malloc(sizeof(mslsl_t)))) {
            return false;
        }
        
        mslsl_node_t *nn;
        if (!(nn = malloc(sizeof(mslsl_node_t)))) {
            free(list[0]);
            list[0] = NULL;
            return false;
        }
        
        nn->ptr = ptr;
        nn->next = NULL;
        nn->prev = NULL;
        
        list[0]->first = nn;
        list[0]->last = nn;
        list[0]->length = 1;
    }
    else {
        mslsl_node_t *c = list[0]->first, *nn;
        bool append = false;
        
        if (index >= 0 && index >= (int) list[0]->length) return false;
        
        if (index >= 0) for (int i = 0; i < index; ++i) if (!c) return false; else c=c->next;
        else append = true;
        
        if (!(nn = malloc(sizeof(mslsl_node_t)))) {
            return false;
        }
        
        nn->ptr = ptr;
        
        if (!append) {
            // [  XX CC YY ]
            // [  XX~NN~CC YY]
            
            nn->prev = c->prev;
            nn->next = c;  
            if (c->prev) c->prev->next = nn;
            c->prev = nn;
            if (index == 0) list[0]->first = nn;
        }
        else {
            // [  XX CC  ]
            // [  XX CC~NN~!]
            list[0]->last->next = nn;
            nn->prev = list[0]->last;
            nn->next = NULL;
            list[0]->last = nn;
        }
        ++list[0]->length;
    }
    return true;
}
#ifdef MUSALSIL_EMBEDED_TEST
static void test_mslslInsert(){
    mslsl_t *c = NULL;
    char *result[] = {"zeroth", "first", "second", "third", "third.half", "fourth"}, i = 0;
    
    {// General Tests
        assert(mslslInsert(&c, "first", -1));
        assert(mslslInsert(&c, "third", -1));
        assert(mslslInsert(&c, "second", 1));
        assert(mslslInsert(&c, "fourth", -1));
        assert(mslslInsert(&c, "zeroth", 0));
        assert(mslslInsert(&c, "third.half", 4));
        assert(checklength(&c, 6));
        for (mslsl_node_t *n = c->first; n; n=n->next, ++i) assert(!strcmp(n->ptr, result[(int)i]));
        assert(checklength(&c, 6));
        mslslFree(&c);
    }        
    
    {// Testing ptr == NULL
        assert(mslslInsert(&c, NULL, -1));
        assert(mslslInsert(&c, "xx", -1));
        assert(!c->first->ptr);
        assert(!strcmp(c->first->next->ptr, "xx"));
        assert(checklength(&c, 2));
        mslslFree(&c);
    }
}
#endif

// MACRO TESTS
#ifdef MUSALSIL_EMBEDED_TEST
static void test_mslslMacros(){
    mslsl_t *c = NULL;
    //char *result[] = {"zeroth", "first", "second", "third", "third.half", "fourth"};
    
    assert(mslslInsert(&c, (int[1]){1}, -1));
    assert(mslslInsert(&c, (int[1]){4}, -1));
    assert(mslslInsert(&c, (int[1]){3}, 1));
    assert(mslslInsert(&c, (int[1]){5}, -1));
    assert(mslslInsert(&c, (int[1]){1}, 0));
    assert(mslslInsert(&c, (int[1]){1}, 4));
    
    int total = 0;
    MSLSL_FOR(&c) total += *((int *)MSLSL_PTR);
    assert(total == 15);
    
    MSLSL_FOR(&c) total += *((int *)MSLSL_NODE->ptr);
    assert(total == 30);
    
    mslslFree(&c);
    
    MSLSL_FOR(&c) puts("hello! this should not be posible..");
}
#endif



bool mslslDelete(mslsl_t **list, int index){/*
USAGE
    This function removes the node at `index' from the linked list `list'.
    
PARAMETER DESCRIPTION
    list        This pointer must point to an address of a linked list or to 
                NULL.
    index       The index of the node in the list. if `index' is -1, the last 
                node is selected.

FAILURE
    This function may fail if index is out of bounds, or if list is empty.
    
RETURN
    This function returns true on success and false on failure.
*/
    if (!list[0]) return false;
    
    mslsl_node_t *c = list[0]->first;
    if (index >= 0) for (int i = 0; i < index; ++i) if (!c) return false; else c=c->next;
    else c = list[0]->last;
    
    if (c->next) c->next->prev = c->prev; 
    if (c->prev) c->prev->next = c->next;
    
    if (c == list[0]->first) list[0]->first = c->next;
    else if (c == list[0]->last) list[0]->last = c->prev;
    
    free(c);
    
    if ((--list[0]->length) == 0) mslslFree(list);

    return true;
}
#ifdef MUSALSIL_EMBEDED_TEST
static void test_mslslDelete(){
    mslsl_t *c = NULL;
    
    {// General Tests
        assert(mslslInsert(&c, "a", -1));
        assert(mslslInsert(&c, "b", -1));
        assert(mslslInsert(&c, "c", -1));
        assert(mslslInsert(&c, "d", -1));
        assert(mslslInsert(&c, "e", -1));
        assert(mslslInsert(&c, "f", -1));
        assert(mslslDelete(&c, 0));
        assert(checklength(&c, 5));
        assert(!strcmp("b", c->first->ptr));
        assert(mslslDelete(&c, -1));
        assert(!strcmp("e", c->last->ptr));
        assert(mslslDelete(&c, 3));
        assert(!strcmp("d", c->last->ptr));
        assert(mslslDelete(&c, 1));
        assert(!strcmp("b", c->first->ptr));
        assert(!strcmp("d", c->first->next->ptr));
        assert(!c->first->next->next);
        assert(checklength(&c, 2));
        assert(mslslDelete(&c, -1));
        assert(checklength(&c, 1));
        assert(c->last == c->first );
        mslslFree(&c);
    }
    
    {// Auto ?freement?
        assert(mslslInsert(&c, "a", -1));
        assert(mslslDelete(&c, -1));
        assert(!mslslDelete(&c, -1)); // over deleting
    }
}
#endif

mslsl_node_t* mslslGet(mslsl_t **list, int index){/*
USAGE
    This function retrieves the node at `index' from the linked list `list'.
    
PARAMETER DESCRIPTION
    list    This pointer must point to an address of a linked list or to NULL.
    index   The index of the node in the list. if `index' is -1, the last node 
            is selected.

NOTE
    Consider using the specialized for loop MACRO `MSLSL_FOR' instead of
    this function if you want to iterate over all the elements in a list.
    (`MSLSL_FOR' gives you linear time iteration with respect to length O(n) 
    while interation using this function will result in O(n?) where ? is the
    additive version of the factorial (!)).

FAILURE
    This function may fail if index is out of bounds, or if list is empty.
    
RETURN
    This function returns true on success and false on failure.
*/
    mslsl_node_t *c;
    if (!list[0]) return NULL;
    if (index >= 0) {
        if ((unsigned int) index < list[0]->length/2) {
            c = list[0]->first;
            for (int i = 0; i < index; ++i) if (!c) return NULL; else c=c->next;
        }
        else {
            c = list[0]->last;
            for (int i = list[0]->length-1; i != index; --i) if (!c) return NULL; else c=c->prev;
        }
    }
    else c = list[0]->last;
    return c;
}
#ifdef MUSALSIL_EMBEDED_TEST
static void test_mslslGet(){
    mslsl_t *c = NULL;
    assert(mslslInsert(&c, "a", -1));
    assert(mslslInsert(&c, "b", -1));
    assert(mslslInsert(&c, "c", -1));
    assert(mslslInsert(&c, "d", -1));
    assert(mslslInsert(&c, "e", -1));
    assert(mslslInsert(&c, "f", -1));
    assert('a' == *((char *)mslslGet(&c, 0)->ptr));
    assert('b' == *((char *)mslslGet(&c, 1)->ptr));
    assert('c' == *((char *)mslslGet(&c, 2)->ptr));
    assert('d' == *((char *)mslslGet(&c, 3)->ptr));
    assert('e' == *((char *)mslslGet(&c, 4)->ptr));
    assert('f' == *((char *)mslslGet(&c, 5)->ptr));
    assert('f' == *((char *)mslslGet(&c, -1)->ptr));
    mslslFree(&c);
}
#endif

bool mslslSwap(mslsl_t **list, int a, int b){/*
USAGE
    This function swaps the locations of two nodes.
    
PARAMETER DESCRIPTION
    list        This pointer must point to an address of a linked list or to 
                NULL.
    a           The index of the first. if `index' is -1, the last node is 
                selected.
    b           The index of the first. if `index' is -1, the last node is 
                selected.

FAILURE
    This function may fail if index is out of bounds, or if list is empty.
    
RETURN
    This function returns true on success and false on failure.
*/
    mslsl_node_t *na, *nb;
    void *ptr;

    if (!list[0]) return false;
    
    if (    !(na = mslslGet(list, a)) 
        ||  !(nb = mslslGet(list, b))) return false;

    ptr =  na->ptr;
    na->ptr = nb->ptr;
    nb->ptr = ptr;

    return true;
}
#ifdef MUSALSIL_EMBEDED_TEST
static void test_mslslSwap(){
    mslsl_t *c = NULL;
    char *result[] = {"b", "f", "c", "d", "a", "e"}, i = 0;
    
    assert(mslslInsert(&c, "a", -1));
    assert(mslslInsert(&c, "b", -1));
    assert(mslslInsert(&c, "c", -1));
    assert(mslslInsert(&c, "d", -1));
    assert(mslslInsert(&c, "e", -1));
    assert(mslslInsert(&c, "f", -1));
    assert(mslslSwap(&c, 0, -1)); // f 
    assert(mslslSwap(&c, 0, 1));
    assert(mslslSwap(&c, 5, 4));
    for (mslsl_node_t *n = c->first; n; n=n->next, ++i) assert(!strcmp(n->ptr, result[(int)i]));
    assert(checklength(&c, 6));
    mslslFree(&c);
}
#endif


mslsl_t* mslslExtract(mslsl_t **list, int index, int length){/*
USAGE
    This function returns a new list containing a subset of the original
    list.
    
PARAMETER DESCRIPTION
    list        This pointer must point to an address of a linked list or to 
                NULL.
    index       The index of the first. if `index' is -1, the last node is 
                selected.
    length      The number of nodes to include. if `length' is -1, all nodes 
                after the `index'th node are included.

FAILURE
    This function may fail if index is out of bounds.
    
RETURN
    This function returns a list pointer on success and NULL on failure. 
    NULL may also be returned if the resulting list is empty.
    
    The returned list must be freed using `mslslFree' or `mslslDelete'.
*/
    if (!list[0]) return NULL;
    
    mslsl_t *sub = NULL;
    mslsl_node_t *c = list[0]->first;

    if (index >= 0) for (int i = 0; i < index; ++i) if (!c) return NULL; else c=c->next; //redundant
    else c = list[0]->last;
    
    if (length == -1) length = list[0]->length;
    
//    printf("%d %d %s\n", from, to, (char *) c->ptr);
    
    for (int i = 0;i < length && c; ++i, c=c->next) if (!mslslInsert(&sub, c->ptr, -1)) {
        mslslFree(&sub);
        return NULL;
    }
    return sub;
}
#ifdef MUSALSIL_EMBEDED_TEST
static void test_mslslExtract(){
    mslsl_t *c = NULL, *d;
    int i;
    
    assert(mslslInsert(&c, "a", -1));
    assert(mslslInsert(&c, "b", -1));
    assert(mslslInsert(&c, "c", -1));
    assert(mslslInsert(&c, "d", -1));
    assert(mslslInsert(&c, "e", -1));
    assert(mslslInsert(&c, "f", -1));
    

    i = 0;
    assert(d = mslslExtract(&c, 1, -1));
    MSLSL_FOR(&d) assert(!strcmp(MSLSL_PTR, ((char *[]) {"b", "c", "d", "e", "f"})[i++]));
    assert(checklength(&d, 5));
    mslslFree(&d);
    
    i = 0;
    assert(d = mslslExtract(&c, 0, c->length-1));
    MSLSL_FOR(&d) assert(!strcmp(MSLSL_PTR, ((char *[]) {"a", "b", "c", "d", "e"})[i++]));
    assert(checklength(&d, 5));
    mslslFree(&d);
    
    i = 0;
    assert(d = mslslExtract(&c, 2, 2));
    MSLSL_FOR(&d) assert(!strcmp(MSLSL_PTR, ((char *[]) {"c", "d"})[i++]));
    assert(checklength(&d, 2));
    mslslFree(&d);
    
    i = 0;
    assert(d = mslslExtract(&c, -1, -1));
    assert(!strcmp(d->first->ptr, "f"));
    assert(checklength(&d, 1));
    mslslFree(&d);

    
    mslslFree(&c);
}
#endif

mslsl_t* mslslClone(mslsl_t **list){/*
USAGE
    Duplicate a list.
    
PARAMETER DESCRIPTION
    list        This pointer must point to an address of a linked list or to 
                NULL.

FAILURE
    This function fails if memory allocation for the new list fails
    
RETURN
    This function returns a pointer to the new list if the function succeeds 
    and NULL on failure. This function will also return `NULL' if passed an 
    empty list.
*/
    mslsl_t* n = NULL;
    if (!list[0]) return NULL;
    MSLSL_FOR(list) if (!mslslInsert(&n, MSLSL_PTR, -1)) {
        mslslFree(&n);
        break;
    }
    return n;
}
#ifdef MUSALSIL_EMBEDED_TEST
static void test_mslslClone(){
    mslsl_t *c = NULL, *d;
    int i;
    
    assert(mslslInsert(&c, "a", -1));
    assert(mslslInsert(&c, "b", -1));
    assert(mslslInsert(&c, "c", -1));
    assert(mslslInsert(&c, "d", -1));
    assert(mslslInsert(&c, "e", -1));
    assert(mslslInsert(&c, "f", -1));
    
    i = 0;
    assert(d = mslslClone(&c));
    assert(checklength(&d, 6));
    MSLSL_FOR(&d) assert(!strcmp(MSLSL_PTR, ((char *[]) {"a", "b", "c", "d", "e", "f"})[i++]));
    mslslFree(&d);
    mslslFree(&c);
}
#endif

bool mslslAssimilate(mslsl_t **list, mslsl_t **sublist, int index){/*
USAGE
    This function assimilates one list to another by inserting it at a 
    specific point in the Assimilating list. This inserting is done by 
    unlinking `list' at `index' and attaching `sublist's in between.
    
    When the function attaches `sublist' to the list, it effectively 
    merges the two lists togeter, and `sublist' as a list object no 
    longer exists. (Its memory is freed and the pointer is pointed 
    at null).
    
    If you need `sublist' after this operation, use mslsDup to duplicate 
    the list beforehand.
    
PARAMETER DESCRIPTION
    list        A list in which `sublist' is assimilated. This pointer must 
                point to an address of a linked list or to NULL.
    sublist     This pointer must point to an address of a linked list or 
                to NULL.
    index       The index at which . if `index' is -1, the last node is 
                selected.

FAILURE
    1. If `*list' is NULL, and `index' is not -1.
    This function may fail if index is out of bounds, or if list is empty.
    
RETURN
    This function returns true on success and false on failure.
*/
    
    mslsl_node_t *node;
    
    if (!sublist[0]) return true; // noop
    
    if (!mslslInsert(list, NULL, index)
    || !(node = mslslGet(list, index))) {return false;}
    

    {// main list endings linking    
        if (node->prev) node->prev->next = sublist[0]->first;
        if (node->next) node->next->prev = sublist[0]->last;
    }
    {// updating main lists pointers
        if (list[0]->first == node) list[0]->first = sublist[0]->first; 
        if (list[0]->last == node) list[0]->last = sublist[0]->last;
    }
    
    {// sublist endlings linking
        if (sublist[0]->first) sublist[0]->first->prev = node->prev;
        else sublist[0]->first->prev = NULL;
        
        if (sublist[0]->last) sublist[0]->last->next = node->next;
        else sublist[0]->last->next = NULL;
    }    
    
    // updating length
    
    list[0]->length += (sublist[0]->length - 1);
    
    {// removing temparary node and sublist remnants
        node->prev = node->next = NULL;
        sublist[0]->length = 1;
        sublist[0]->first=node;
        sublist[0]->last=node;
        mslslFree(sublist);
    }
    
    return true;    
}
#ifdef MUSALSIL_EMBEDED_TEST
static void test_mslslAssimilate(){
    mslsl_t *c = NULL, *d = NULL, *e = NULL;
    
    assert(mslslInsertNA(&c, (void *[]) {"d", "e", "f", NULL}, -1));
    assert(checklength(&c, 3));

    assert(mslslInsertNA(&d, (void *[]) {"a", "b", "c", NULL}, -1));
    assert(mslslAssimilate(&c, &d, 0));
    assert(checklength(&c, 6));
    assert(d == NULL);
    
    assert(mslslInsertNA(&d, (void *[]) {"j", "k", "l", NULL}, -1));    
    assert(mslslAssimilate(&c, &d, -1));
    assert(checklength(&c, 9));
    
    assert(mslslInsertNA(&d, (void *[]) {"g", "h", "i", NULL}, -1));
    assert(mslslAssimilate(&c, &d, 6));
    assert(checklength(&c, 12));
    
    char z[2] = "a";
    MSLSL_FOR(&c) {
        assert(!strcmp(MSLSL_PTR, z));
        ++z[0];
    }
    
    mslslFree(&c);
    
    

    d = NULL;
    assert(mslslAssimilate(&c, &d, -1));
    assert(!c);
    
    c = NULL;
    mslslInsertNA(&d, (void *[]) {"3", "2", "1", NULL}, -1);
    assert(mslslAssimilate(&c, &d, -1));
    assert(checklength(&c, 3));
    assert(mslslAssimilate(&c, &e, -1));
    assert(checklength(&c, 3));
    mslslFree(&c);
    
    c = NULL;
    d = NULL;
    mslslInsertNA(&d, (void *[]) {"3", NULL}, -1);
    assert(mslslAssimilate(&c, &d, -1));
    assert(checklength(&c, 1));
    mslslFree(&c);
}
#endif


void mslslFree(mslsl_t **list){/*
USAGE
    This function frees the resources allocated for the list.
    
PARAMETER DESCRIPTION
    list        This pointer must point to an address of a linked list or to 
                NULL.
                
RETURN
    None.
*/
    if (!list[0]) return;
    if (list[0]->length) {
        for (mslsl_node_t *p = list[0]->first; p; p = p->next) free(p->prev);
        free(list[0]->last);
    }
    free(list[0]);
    list[0]=NULL;
}
// NOTE: failure due to this function shows up in valgrind -> no need for tests




bool mslslExport(mslsl_t **list, void ***buf, int length){/*
USAGE
    This function stores all pointers in `list' into `buf' provided that it
    is long enough (`length' denotes `buf' length). If buf is NULL, this 
    function will allocate the nessary memory on the user's behalf.
    
    The resulting list is NULL-terminated.
    
    
EXAMPLE
    
        void **array;
        mslslExport(&list, &array, 0);
        
    stores the content of `list' as a NULL-terminated list in `array'.
    
NOTE
    If this function allocates memory on your behalf, you should free it using
    stdlib's default `free' function. Do not use mslslFree to free anything 
    except `mslsl_t' pointers.
    
FAILURE
    This function fails if the provided buffer is not long enough, or if 
    memory allocation fails.
    
RETURN
    True on success, false otherwise.
*/
    void **out = *buf;
    int i = 0;
    
        
    if (!out) {// if Export should allocate memory on behalf of the user
        if (!(out = malloc(sizeof(void *)*(list[0]->length+1)))) return false;
    }
    else if (!length || (unsigned int) length < (list[0]->length+1)) return false;
    
    
    MSLSL_FOR(list) {
        out[i++] = MSLSL_PTR;
    }
    out[i] = NULL;
    
    *buf=out;
    
    return true;
}
#ifdef MUSALSIL_EMBEDED_TEST
static void test_mslslExport(){
    mslsl_t *c = NULL;
    void **out1 = NULL, **out2 = malloc(sizeof(void *)*10);
    
    assert(mslslInsertNA(&c, (void *[]) {"1", "2", "3", NULL}, -1));
    assert(checklength(&c, 3));
    assert(mslslInsertNA(&c, (void *[]) {"7", "8", "9", NULL}, -1));
    assert(checklength(&c, 6));
    assert(mslslInsertNA(&c, (void *[]) {"4", "5", "6", NULL}, 3));
    assert(checklength(&c, 9));

    //for(int i=0;out1[i];++i)puts(out1[i]);puts("");
    assert(mslslExport(&c, &out1, 0));   
    assert(mslslExport(&c, &out2, 10));
    assert(!mslslExport(&c, &out2, 9));
    assert(!mslslExport(&c, &out2, 0));
    
    int i = 0;
    MSLSL_FOR(&c) {
        assert(MSLSL_PTR == out1[i] && MSLSL_PTR == out2[i]);
        ++i;
    }
    assert(i==9);
    
    free(out1);
    free(out2);
    mslslFree(&c);
}
#endif



#ifdef MUSALSIL_EMBEDED_TEST
int main(){/*
      __                                                 __
     |_ |    ___                 ___                    |_ |    ___   ___
      /_/   |   |               |   |                    /_/   |   | |   |
            |   |  ___ ___ ___  |   |  ___ ___ ___    _______  |   | |   |
     ___    |   | |   |   |   | |   | |   |   |   |  /   _   | |   | |   |
    |   |   |   |_|   |   |   |_|   |_|   |   |   |_|   | |  |_|   | |   |
    |   |___|                                           |_|        | |   |
    |            _/___/___/___/_____/_/___/___/___/__________/_____/ |___|
    |___________/  Khalid Umar <khalid.umar@zoho..com> مع تحياتي، خالد عمر

    Musalsil: a simple library linked list library.
    
INTRODUCTION
    This library aims to be a simple, fast, robust library for dealing with 
    linked lists. To this end, the library:
    
        1.  provides a simple API design: Only two functions are required to 
            handle linked lists.
        2.  employs a basic structure design for list nodes: prev, next, and 
            ptr.
        3.  is designed to avoid sequential access and thus improve lookup 
            time (Not implemented yet, caching or short lookup)
        4.  includes automated tests.

DEPENDANCIES
    0.  Compiled against the c1x standard.
    1.  stdbool.h (c standard library) must be included before musalsil.h
        
BASIC USAGE
    To use this library, you only need three functions and basic understanding 
    of a simple struct. linked lists are typed as `mslsl_t' and must be declared 
    as pointers:
    
        mslsl_t *list = NULL;
    
    +----------------------------------------------------------------------+
    | IMPORTANT, `list' MUST POINT TO NULL OR A VALID `mslsl_t' STRUCTURE. |
    | DECLARING A VARIABLE WITHOUT INITIALIZING IT TO `NULL' IS A COMMON   |
    | MISTAKE. BE CAREFUL.                                                 |
    +----------------------------------------------------------------------+
    
    To add some pointer to the list, simply call `mslslInsert':
    
        mslslInsert(&list, "This string's address is going in", -1);
    
    Musalsil does not require you to initialize a list before passing it to the
    functions it provides. 
    
    `mslslInsert' takes a list pointer's address as its first argument, any 
    pointer as its second, and the index at which the pointer should be 
    inserted as its third. If -1 is provided as the third argument, the 
    provided pointer will be appended to the list. Refer `mslslInsert's 
    documentation for more  information.
    
    It should be noted that ALL Musalsil functions and macros take list pointer 
    address (mslsl_t **) as their linked list parameter.
    
    To remove a node from the list, call `mslslDelete' passing the list pointer 
    address and an index:
    
        mslslDelete(&list, -1);
        
    This deletes the last node in the list. if it was the only node, the list 
    itself is automaticly freed. (You can use `mslslFree' to free the list 
    regardless of its content. see the complete api list.)
    
    To interact with a stored pointer, `mslslGet':
    
        mslsl_node_t *node = mslslGet(&list, -1);
    
    Nodes are of type (mslsl_node_t *) and are defined as follows:
    
        typedef struct mslsl_node_s_ {
            struct mslsl_node_s_ *next;
            struct mslsl_node_s_ *prev;
            void *ptr;
        } mslsl_node_t;
    
    `node->ptr' in the last example would resolve to the pointer that was 
    provided to `mslslInsret' in the first example, provided that `mslslDelete' 
    was not called.
    
    It should be noted that you can avoid using a variable to hold the node if 
    its a one-off access. Simply use the function directly:
    
        mslslGet(&list, -1)->ptr
    
    As for list iteration, Musalsil provides a "for-in" loop construct 
    implemented as a preprocessor macro. `MSLS_FOR' is equivalent to C's `for' 
    keyword, and thus has the same syntactical properties (i.e. braces are 
    required for multi-line loop bodies while optional for single liners). The 
    primary difference is that `MSLS_FOR' takes one argument only, a list 
    pointer address.
    
    Within a `MSLS_FOR' loop, two additional macros can be used: `MSLS_NODE' 
    and `MSLS_PTR' where the first is current iteration node and the second is 
    the pointer of that node.
    
    To print the addresses stored in a loop, one might use the following:
    
        MSLSL_FOR(&list) pirntf("%p\n", MSLS_PTR);
    
    Important aspects to consider when using MSLSL_FOR:
        1.  Continuing to iterate after modifing a list is undefined behaviuor.
        2.  For the time being, a list must have at least one element.
    
    See the api list for all the functions this library defines. 

*/
    test_mslslInsert();
    test_mslslInsertNA();
    test_mslslDelete();
    test_mslslGet();
    test_mslslSwap();
    test_mslslMacros();
    test_mslslExtract();
    test_mslslClone();
    test_mslslAssimilate();
    test_mslslExport();
    if ((size_t)main^(size_t)main) printls(NULL, NULL); // سكت حنة جيسيسي
}
#endif
