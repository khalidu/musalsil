CC=gcc
CFLAGS= -Wall -Wextra -g -std=c1x
NAME=musalsil
CFILES=musalsil.c
HFILES=musalsil.h

dev.tests: $(CFILES) $(HFILES)
	$(CC) -DMUSALSIL_EMBEDED_TEST $(CFLAGS)  $(CFILES) -o dev.tests
	
test: dev.tests
	@./dev.tests
	@echo "ok"
	@-rm *.tests
    
debug: dev.tests
	gdb ./dev.tests
    
example:  $(CFILES) $(HFILES) example.c
	$(CC) $(CFLAGS) $(CFILES) example.c -o example
	
memcheck: dev.tests
	valgrind --tool=memcheck --leak-check=full --show-reachable=yes ./dev.tests
	
	

