#include <stdio.h>
#include <stdbool.h>
#include "musalsil.h"

int main(){
    mslsl_t *list = NULL;
    
    mslslInsertNA(&list, (void *[]){ "السلام عليكم", "ارجو ان يكون في هذه المكتبة مرادك وأن تسهل عليك أمرك", "مع تحياتي،", "خالد عمر", NULL}, -1);
    
    MSLSL_FOR(&list) puts((char *) MSLSL_PTR);
    
    mslslFree(&list);
    
    return 0;
}
