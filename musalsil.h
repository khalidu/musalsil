#ifndef MUSALSIL_H
#define MUSALSIL_H
/*
      __                                                 __
     |_ |    ___                 ___                    |_ |    ___   ___
      /_/   |   |               |   |                    /_/   |   | |   |
            |   |  ___ ___ ___  |   |  ___ ___ ___    _______  |   | |   |
     ___    |   | |   |   |   | |   | |   |   |   |  /   _   | |   | |   |
    |   |   |   |_|   |   |   |_|   |_|   |   |   |_|   | |  |_|   | |   |
    |   |___|                                           |_|        | |   |
    |            _/___/___/___/_____/_/___/___/___/__________/_____/ |___|
    |___________/  Khalid Umar <khalid.umar@zoho..com> مع تحياتي، خالد عمر
    

    Copyright (c) 2013  Adnal Technologies, Kuwait

    This work is licensed under the terms and conditions of the Adnal-WPL 
    (Adnal Waqf Public License) which should be included with this program. 
    You can obtain a copy by visiting: http://adnal.net.kw/legal/wpl

    Compliance with all terms and conditions set forth in the Adnal-WPL 
    license is a requirement to exercise the rights it defines. If you are 
    unable to fully comply or are unwilling to do so, you may not exercise 
    the rights defined therein.

    This work is provided WITHOUT WARRANTY. The right holders DISCLAIM ANY 
    DAMAGE that may arise from the use of this work. USE AT YOUR OWN RISK.

    If translations of the Adnal-WPL license exist, the Arabic version is 
    taken to be correct and legally binding.

*/

typedef struct mslsl_node_s_ {
    struct mslsl_node_s_ *next;
    struct mslsl_node_s_ *prev;
    void *ptr;
} mslsl_node_t;

typedef struct mslsl_s_ {
    mslsl_node_t *last;
    mslsl_node_t *first;
    unsigned int length; //todo XXX XXX add to the functions and fix up mslslInsert's nth sigfult
    /*add caching maybe? or midpoint refrence to speed up access
        always insure that at most 100 entries must be searched by using a 
        relloced array that caches pointers and their indexes
            pros:
                -- fast access (reads/ swaps/ etc) < O(100)
            cons:
                -- if the list is large, inserts will take O(n), solution? 
                   disable caching and switch back to sequential access if array
                   becomes too large?
        processor-cache-like cache?
    */
} mslsl_t;

bool mslslInsert(mslsl_t **list, void *ptr, int index);
bool mslslInsertNA(mslsl_t **list, void *ptrs[], int index);
mslsl_t* mslslClone(mslsl_t **list);

bool mslslDelete(mslsl_t **list, int index);
void mslslFree(mslsl_t **list);

bool mslslSwap(mslsl_t **list, int a, int b);
mslsl_node_t* mslslGet(mslsl_t **list, int index);

mslsl_t* mslslExtract(mslsl_t **list, int index, int length);
bool mslslAssimilate(mslsl_t **list, mslsl_t **sublist, int index);




#define MSLSL_FOR(LIST) for (mslsl_node_t *mslsl_n__ = ((LIST)[0]?(LIST)[0]->first:NULL); mslsl_n__; mslsl_n__ = mslsl_n__->next)
#define MSLSL_PTR (mslsl_n__->ptr)
#define MSLSL_NODE (mslsl_n__)

#endif
